---
title: "Guia2-R"
author: "Josefa Hillmer"
date: "14-08-2020"
output:
  pdf_document:
    
    highlight: zenburn
    toc: yes
    number_sections: true
    toc_depth: 2
  html_document:
    highlight: default
    number_sections: true
    theme: cosmo
    toc: yes
    toc_depth: 2
  word_document: default
---

# Continuación del dataset anterior 
## Cargar dataset generado anteriormente

Estudio de datos respecto a problemas cardíacos

```{r cargar data}
filename<-"dataset_clean.csv"
dataset<-read.csv(filename, sep=",")
```

## Selección de variables cuantitativas y cualitativas
Se selecciona las variables cuantitativas y dos variables cualitativas para compararlas.
Se selecciona la variable genero y la variable ataque cardíaco
```{r variables}
# Variables cuantitativas
cuantitativas <- dataset[, c("trestbps", "chol", "thalach", "oldpeak"), ]

# Dos variables cualitativas
cualitativas <- dataset[, c("sex", "target", "thal", "fbs", "restecg"), ]
# Para diferenciar las variables
cualitativas$sex[cualitativas$sex==1] <- "Hombre"
cualitativas$sex[cualitativas$sex==0] <- "Mujer" 
cualitativas$target[cualitativas$target==0] <- "Sin ataque cardíaco"
cualitativas$target[cualitativas$target==1] <- "Ataque cardíaco"
```

## Carga de librerías
A continuación se presentan las librerías a utilizar en este documento.
```{r cargar librerias}
library(ggplot2)
#library(carData)
library(gridExtra)
library(grid)
library(plyr)
library(GGally)
```

## Histograma
El gráfico de barras clásico o histograma utiliza barras horizontales o verticales (gráfico de columnas) para mostrar comparaciones numéricas discretas entre categorías. 

```{r, histograma}
# Función crea histograma
crea_histograma <- function(column, titulo, variable, w=3, legend=FALSE){
  if(legend){
    show_legend = theme(legend.position = "right")
  }else{
    show_legend = theme(legend.position = "none")
  }
  
  ggplot(data=cuantitativas, aes(column)) +
    geom_histogram(binwidth = w, color = "black", aes(fill=variable)) +
    xlab(titulo) +
    ylab("Frecuencia") +
    show_legend +
    ggtitle(paste("Histograma de", titulo)) +
    geom_vline(data=cuantitativas, aes(xintercept = mean(na.omit(column))),
               linetype="dashed", color = "blue")
}
```

### Sexo
```{r histograma sexo, warning=FALSE}

# Llama a función histograma 
trestbps_sex <- crea_histograma(cuantitativas$trestbps, "Trestbps", cualitativas$sex)
chol_sex <- crea_histograma(cuantitativas$chol, "Chol", cualitativas$sex, 5)
thalach_sex <- crea_histograma(cuantitativas$thalach, "Thalach", cualitativas$sex)
oldpeak_sex <- crea_histograma(cuantitativas$oldpeak, "Oldpeak", cualitativas$sex, 1, TRUE)

grid.arrange(trestbps_sex + ggtitle(""),
             chol_sex + ggtitle(""),
             thalach_sex + ggtitle(""),
             oldpeak_sex + ggtitle(""),
             nrow=2,
             top=textGrob("Sex", 
                          gp=gpar(dontsize=14)))
```
Se puede visualizar en los 4 gradicos, dentro de los pacientes, hay mayor cantidad de hombre que mujeres. 
Se observa que ma media de Trestbps es aproximadamente 130, la media de Chol es aproximadamente 250, la media de Thalach es 150 y la media de Oldpeak es 1. En todos los graicos se ve que la media se encuentra aproximadamente al medio de los datos. 

### Ataque cardíaco
Se puede visualizar en el grafico que hay una gran cantidad de pacientes que tuvieron ataque cardíaco a comparación a los que no han tenido.
```{r histograma target, warning=FALSE}
trestbps_target <- crea_histograma(cuantitativas$trestbps, "Trestbps", cualitativas$target)
chol_target <- crea_histograma(cuantitativas$chol, "Chol", cualitativas$target, 10)
thalach_target <- crea_histograma(cuantitativas$thalach, "Thalach", cualitativas$target)
oldpeak_target <- crea_histograma(cuantitativas$oldpeak, "Oldpeak", cualitativas$target, 1, TRUE)

grid.arrange(trestbps_target + ggtitle(""),
             chol_target + ggtitle(""),
             thalach_target + ggtitle(""),
             oldpeak_target + ggtitle(""),
             nrow=2,
             top=textGrob("Target", 
                          gp=gpar(dontsize=14)))
```
Se puede visualizar en los 4 grafico que hay una gran cantidad de pacientes que tuvieron ataque cardíaco a comparación a los que no han tenido.
También se puede observar que la media de Trestbps es aproximadamente 130, la media de Chol es aproximadamente 250, la media de Thalach es 150 y la media de Oldpeak es 1. En todos los graicos se ve que la media se encuentra aproximadamente al medio de los datos. 

## Grafico de densidad
Un gráfico de densidad visualiza la distribución de datos en un intervalo.
```{r densidad, warning=FALSE}
crea_densidad <-function(column, titulo, variable, legend=FALSE){
  if(legend){
    show_legend =theme(legend.position ="right")
    }else{
      show_legend =theme(legend.position="none")
      }
  
  ggplot(data=cuantitativas, aes(column, color=variable, fill=variable)) +
    geom_density(alpha=.3) +
    xlab(titulo) +
    ylab("Frecuencia") +
    show_legend +
    ggtitle(paste("Diagrama de densidad de", titulo)) +
    geom_vline(data=cuantitativas, 
               aes(xintercept = mean(na.omit(column)),
                   color=variable),
               linetype="dashed", color="blue")
}
```

### Sexo
```{r densidad sexo, warning=FALSE}

# Llama a función densidad
trestbps_sex <- crea_densidad(cuantitativas$trestbps, "Trestbps", cualitativas$sex)
chol_sex <- crea_densidad(cuantitativas$chol, "Chol", cualitativas$sex)
thalach_sex <- crea_densidad(cuantitativas$thalach, "Thalach", cualitativas$sex)
oldpeak_sex <- crea_densidad(cuantitativas$oldpeak, "Oldpeak", cualitativas$sex, TRUE)

grid.arrange(trestbps_sex + ggtitle(""),
             chol_sex + ggtitle(""),
             thalach_sex + ggtitle(""),
             oldpeak_sex + ggtitle(""),
             nrow=2,
             top=textGrob("Sex", 
                          gp=gpar(dontsize=14)))


```
Se puede visualizar en los graficos ciertas similitudes. En Trestbps el máximo de frecuencia entre ambos sexos es practicamente igual. En Chol el máximo de las mujeres es menor al maximo de los hombres. Tanto en Thalach como en Oldpeak las mujeres tienen un frecuencia maxima mayor a comparación de los hombres. 

### Ataque cardíaco
```{r densidad ataque cardiaco, warning=FALSE}

trestbps_target <- crea_densidad(cuantitativas$trestbps, "Trestbps", cualitativas$target)
chol_target <- crea_densidad(cuantitativas$chol, "Chol", cualitativas$target)
thalach_target <- crea_densidad(cuantitativas$thalach, "Thalach", cualitativas$target)
oldpeak_target <- crea_densidad(cuantitativas$oldpeak, "Oldpeak", cualitativas$target, TRUE)

grid.arrange(trestbps_target + ggtitle(""),
             chol_target + ggtitle(""),
             thalach_target + ggtitle(""),
             oldpeak_target + ggtitle(""),
             nrow=2,
             top=textGrob("Target", 
                          gp=gpar(dontsize=14)))

```
Se puede visualizar en los graficos ciertas similitudes. En Trestbps el máximo de frecuencia entre ambos es practicamente igual. En Chol el máximo de las personas que tuvieron ataque cardíaco es mayor al maximo de los que no tuvieron. En Thalach se observa que el maximo de la frecuencia de las personas que tivieron ataque cardiaco es mayor a comparacion de los que no y a comparacion del Chol se observa una mayor densidad. En cuanto a Oldpeak se observa que las personas que no tuvieron ataque cardiáco tienen una densidad estable en comparción a los que si tivieron que tienen una densidad mayor. 

## Boxplot o diagrama de bigotes.
Un diagrama de cajas y bigotes se usa para mostrar visualmente grupos de datos numéricos a través de sus cuartiles.
```{r boxplot, warning=FALSE}
# Define función
crea_boxplot <- function(column, titulo, variable, legend=FALSE){
  if(legend){
    show_legend =theme(legend.position ="right")
    }else{
      show_legend =theme(legend.position="none")
      }
  ggplot(data=cuantitativas, aes(variable, column, fill=variable)) +
    geom_boxplot() +
    scale_y_continuous(titulo, breaks=seq(0,30,by=.5)) +
    show_legend
}

```

### Sexo
```{r sexo boxplot, warning=FALSE}
# Llama la función boxplot
trestbps_sex <- crea_boxplot(cuantitativas$trestbps, "Trestbps", cualitativas$sex)
chol_sex <- crea_boxplot(cuantitativas$chol, "Chol", cualitativas$sex)
thalach_sex <- crea_boxplot(cuantitativas$thalach, "Thalach", cualitativas$sex)
oldpeak_sex <- crea_boxplot(cuantitativas$oldpeak, "Oldpeak", cualitativas$sex, TRUE)

grid.arrange(trestbps_sex + ggtitle(""),
             chol_sex + ggtitle(""),
             thalach_sex + ggtitle(""),
             oldpeak_sex + ggtitle(""),
             nrow=2,
             top=textGrob("Sexo", 
                          gp=gpar(dontsize=14)))
```
Datos presentados frente a la variable sexo:

Se puede observar en Trestbps que frente a ambas cualidades son simétricas de acuerdo a sus datos y no tiene valores atípicos.
En Chol se puede considerar ambos casos como casi simétricos, ya que en el caso de los hombres se observa que es muy parecido a Trestbps a comparación de las mujeres que es un poco mas cercano al segundo cuartil. No tienen valores atípicos.
En Thalach en los datos de los hombres se puede observar que se encuentra más simetrico que en el caso de las mujeres que presenta mayor disperción de datos. En las mujeres se puede apreciar un valor atípico al mínimo establecido.
En Oldpeak se puede observar como hay mayor dispersión de datos tanto en hombres como en mujeres, también se aprecia una acumulación de datos en el primer cuartil. En las mujeres se pueden observar dos valores atípicos mayores al máximo establecido.


### Ataque cardíaco
```{r target boxplot, warning=FALSE}
# Llama la función boxplot
trestbps_target <- crea_boxplot(cuantitativas$trestbps, "Trestbps", cualitativas$target)
chol_target <- crea_boxplot(cuantitativas$chol, "Chol", cualitativas$target)
thalach_target <- crea_boxplot(cuantitativas$thalach, "Thalach", cualitativas$target)
oldpeak_target <- crea_boxplot(cuantitativas$oldpeak, "Oldpeak", cualitativas$target, TRUE)

grid.arrange(trestbps_target + ggtitle(""),
             chol_target + ggtitle(""),
             thalach_target + ggtitle(""),
             oldpeak_target + ggtitle(""),
             nrow=2,
             top=textGrob("Target", 
                          gp=gpar(dontsize=14)))
```
Datos presentados frente a la variable target (ataque cardíaco):

Se puede observar en Trestbps que en ambas situaciones (con y sin ataque cardíaco) hay una simétria de acuerdo a sus datos. Con respecto a los ataques cardíacos se encuentra un valor atípico mayor al máximo establecido.
En Chol se puede ver como hay una simétria en sin ataques cardíacos a comparación a los que si tienen, ya que estos se encuentran un poco mas dispersos. Se presentan en ataque cardíaco dos valores atípicos mayores al máximo establecido.
En Thalach los datos en ataque cardiaco se ve una simetría, pero más cercano al tercer cuatil, mientras que en sin ataque cardíaco encontramos los datos un poco mas dispersos. Se encuentran en ataque cardíaco más de un valor atípico menor al mínimo establecido.
En Oldpeak hay una acumulación de datos en las partes menores de las variables debido a que el mínimo se encuentra en el primer cuartil en ambos casos. En las ataque cardíacos se pueden observar tres valores atípicos mayores al máximo establecido.


## Violin Plot
Un diagrama de violín se utiliza para visualizar la distribución de los datos y su densidad de probabilidad. 
```{r violin plot, warning=FALSE}
crea_violin <- function(column, titulo, variable, legend=FALSE){
  if(legend){
    show_legend =theme(legend.position ="right")
    }else{
      show_legend =theme(legend.position="none")
    }
  
  ggplot(data=cuantitativas, aes(variable, column, fill=variable)) +
    geom_violin(aes(color=variable), trim = T) +
    scale_y_continuous(titulo) +
    geom_boxplot(width=0.1) +
    show_legend
}
```

### Sexo
```{r sexo violin, warning=FALSE}
# Llama la función violin plot
trestbps_sex <- crea_violin(cuantitativas$trestbps, "Trestbps", cualitativas$sex)
chol_sex <- crea_violin(cuantitativas$chol, "Chol", cualitativas$sex)
thalach_sex <- crea_violin(cuantitativas$thalach, "Thalach", cualitativas$sex)
oldpeak_sex <- crea_violin(cuantitativas$oldpeak, "Oldpeak", cualitativas$sex, TRUE)

grid.arrange(trestbps_sex + ggtitle(""),
             chol_sex + ggtitle(""),
             thalach_sex + ggtitle(""),
             oldpeak_sex + ggtitle(""),
             nrow=2,
             top=textGrob("Sexo", 
                          gp=gpar(dontsize=14)))
```
Datos presentados frente a la variable sexo:

Se puede observar en Trestbps que la media tanto en hombre como mujeres es la misma (130), el ancho de densidad en ambos es muy paresida. No se encuentran valores atípicos.
En Chol, la media de los hombres (aprox. 225) es menor a la media de las mujeres (aprox. 250), y el ancho de densidad es dintinta, en los hombres esta mas centrada en los costados, mientras que en las mujeres esta en el centro. No hay valores atípicos.
En Thalach se observa como la media de los hombres se encuentra de forma simetrica, la media de los hombres en de 150, mientras que el de las mujeres es mayor aprox. 160. En cuanto a al ancho de su densidad se observa lo contrario a lo sucedido con los valores de Chol. Se encuentra en las mujeres un valor atípico menor al mínimo establecido.
En Oldpeak se observa una media de los hombres es mayor que la de las mujeres, y una anchura de densidad muy parecida también, en ambos esta concentrada en el primer cuartil y mientras se acerca al tercer cuartil está se va alargando. En las mujeres se encuentran dos valores atípicos mayores al máximo establecido.

### Ataque cardíaco
```{r target violin, warning=FALSE}
# Llama la función violin plot
trestbps_target <- crea_violin(cuantitativas$trestbps, "Trestbps", cualitativas$target)
chol_target <- crea_violin(cuantitativas$chol, "Chol", cualitativas$target)
thalach_target <- crea_violin(cuantitativas$thalach, "Thalach", cualitativas$target)
oldpeak_target <- crea_violin(cuantitativas$oldpeak, "Oldpeak", cualitativas$target, TRUE)

grid.arrange(trestbps_target + ggtitle(""),
             chol_target + ggtitle(""),
             thalach_target + ggtitle(""),
             oldpeak_target + ggtitle(""),
             nrow=2,
             top=textGrob("Target", 
                          gp=gpar(dontsize=14)))
```
Datos presentados frente a la variable target (ataque cardíaco):

Se puede observar en Trestbps que tanto con o sin ataque cardíaco la media es la misma (130), el ancho de densidad en ambos es muy paresida. En ataque cardíaco se encuentra un valor atípicos mayor al máximo establecido.
En Chol, la media de ataque cardíaco (aprox. 225) es menor a la media de los que no tienen (aprox. 250), y el ancho de densidad es dintinta, en ataque cardíaco esta mas centrada en los costados, mientras que en sin ataque cardíaco esta en el centro. En ataque cardíaco se observan valores atípicos mayores al máximo establecido.
En Thalach se observa como la media de ataque cardíaco (aprox. 160) es mayor a la media de los que no sufren ataque (aprox. 140). En cuanto a al ancho de su densidad se observa algo muy parecido en lo sucedido con los valores de Chol. Se encuentra en ataque cardíaco valores atípico menor al mínimo establecido.
En Oldpeak se observa una media de ataque cardiaco es muy cercana a 0, en cambio la meida de los que no presentan ataque cardíaco es aproximadamente 1,5. Tienen una anchura de densidad muy parecida, en ambos esta concentrada en el primer cuartil y mientras se acerca al tercer cuartil está se va alargando. En las ataque cardíaco se encuentran tres valores atípicos mayores al máximo establecido.

## Scatterplot
También conocido como gráfico de dispersión, gráfico de puntos, diagrama de XY.
Los scatterplot usan una colección de puntos en coordenadas cartesianas para mostrar valores de dos variables. Al mostrar una variable en cada eje, se puede detectar si existe una relación o correlación entre las dos variables.
```{r scatterplot}

crea_scatterplot <- function(abscisa, ordenada, variable, x, y){
  show_legend=theme(legend.position = "bottom")
  ggplot(data=cuantitativas, aes(x=abscisa, y=ordenada)) + 
    xlab(x) + ylab(y) +
    geom_point(aes(color=variable, shape=variable)) +
    geom_smooth(method ="lm") + show_legend
}

```

```{r, warning=FALSE}
# Llama la función scatterplot

chol_oldpeak_sex <- crea_scatterplot(cuantitativas$oldpeak, cuantitativas$chol,  
                                     cualitativas$sex, "Depresión", "Colesterol")
chol_oldpeak_target <- crea_scatterplot(cuantitativas$oldpeak, cuantitativas$chol, 
                                        cualitativas$target, "Depresión", "Colesterol")

chol_thalach_sex <- crea_scatterplot(cuantitativas$thalach, cuantitativas$chol,
                                     cualitativas$sex, "Frecuencia cardíaca", "Colesterol")
chol_thalach_target <- crea_scatterplot(cuantitativas$thalach, cuantitativas$chol, 
                                        cualitativas$target, "Frecuencia cardíaca", "Colesterol")

trest_chol_sex <- crea_scatterplot(cuantitativas$chol, cuantitativas$trestbps,
                                   cualitativas$sex, "Colesterol", "Presión arterial")
trest_chol_target <- crea_scatterplot(cuantitativas$chol, cuantitativas$trestbps,
                                      cualitativas$target, "Colesterol", "Presión arterial")

trest_thalach_sex <- crea_scatterplot(cuantitativas$thalach, cuantitativas$trestbps,
                                      cualitativas$sex, "Frecuencia cardíaca", "Presión arterial")
trest_thalach_target <- crea_scatterplot(cuantitativas$thalach, cuantitativas$trestbps, 
                                         cualitativas$target, "Frecuencia cardíaca", "Presión arterial")

trest_oldpeak_sex <- crea_scatterplot(cuantitativas$oldpeak, cuantitativas$trestbps, 
                                      cualitativas$sex, "Depresión", "Presión arterial")
trest_oldpeak_target <- crea_scatterplot(cuantitativas$oldpeak, cuantitativas$trestbps,
                                         cualitativas$target, "Depresión", "Presión arterial")

thalach_oldpeak_sex <- crea_scatterplot(cuantitativas$oldpeak, cuantitativas$thalach,
                                        cualitativas$sex, "Depresión", "Frecuencia cardíaca")
thalach_oldpeak_target <- crea_scatterplot(cuantitativas$oldpeak, cuantitativas$thalach, 
                                           cualitativas$target, "Depresión", "Frecuencia cardíaca")
```


### Colesterol vs Depresión

```{r, warning=FALSE}
grid.arrange(chol_oldpeak_sex + ggtitle("Sexo"),
             chol_oldpeak_target + ggtitle("Target"),
             nrow=1,
             top=textGrob("Colesterol vs Depresión", 
                          gp=gpar(dontsize=14)))

```


### Colesterol vs Frecuencia cardíaca
```{r, warning=FALSE}
grid.arrange(chol_thalach_sex + ggtitle("Sexo"),
             chol_thalach_target + ggtitle("Target"),
             nrow=1,
             top=textGrob("Colesterol vs Frecuencia cardíaca", 
                          gp=gpar(dontsize=14)))
```

### Presión arterial vs Colesterol
```{r, warning=FALSE}
grid.arrange(trest_chol_sex + ggtitle("Sexo"),
             trest_chol_target + ggtitle("Target"),
             nrow=1,
             top=textGrob("Presión arterial vs Colesterol", 
                          gp=gpar(dontsize=14)))

```

### Presión arterial vs Frecuencia cardíaca
```{r, warning=FALSE}
grid.arrange(trest_thalach_sex + ggtitle("Sexo"),
             trest_thalach_target + ggtitle("Target"),
             nrow=1,
             top=textGrob("Presión arterial vs Frecuencia cardíaca", 
                          gp=gpar(dontsize=14)))
```

### Presión arterial vs Depresión
```{r, warning=FALSE}
grid.arrange(trest_oldpeak_sex + ggtitle("Sexo"),
             trest_oldpeak_target + ggtitle("Target"),
             nrow=1,
             top=textGrob("Presión arterial vs Depresión", 
                          gp=gpar(dontsize=14)))
```

### Frecuencia cardíaca vs Depresión
```{r, warning=FALSE}
grid.arrange(thalach_oldpeak_sex + ggtitle("Sexo"),
             thalach_oldpeak_target + ggtitle("Target"),
             nrow=1,
             top=textGrob("Frecuencia cardíaca vs Depresión", 
                          gp=gpar(dontsize=14)))
```

## Correlación
La Correlación referencia al movimiento de dos o más variables en torno a una consecuencia provista. Es básicamente cuando dos elementos tienen armonía en su variación, esta armonía es dependiente, es decir, de la estabilización de una depende la posición de la otra. 
```{r correlación, warning=FALSE}
ggpairs(data=cuantitativas, 
title = "Correlación de variables cuantitativas",
upper = list(continuous = wrap("cor", size=5)),
lower = list(continuous = "smooth"))
```
La corelación más baja entre los datos es entre Thalach y Oldpeak, siendo esta negativa de -34.5%, Mientras que las corelaciones más altar es entre Chol y Trestbps de 9%. Esto indica que entre los datos no exite una relación significativa, por lo tanto son independientes entre si.

## Heatmap
```{r}
# Las primeras cuatro columnas de las cuantitativas
matriz <-as.matrix(cuantitativas[1:303,1:4])
# Transpuesta para matriz
trans <-t(matriz)[,nrow(matriz):1]
# Genera mapa de calor
image(1:4,1:303, trans, xlab ="Columnas", ylab ="Variables")
```
Se puede observar que la columna dos tiene un color rojo, a comparación de la 1 y 3 que son de color naranja y la columna 4 que es de color amarillo. Este color rojo indica que tiene valores muy pequeños, mientras que el color amarillo indica que tiene valores más grandes. 






